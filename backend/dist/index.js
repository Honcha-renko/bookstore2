"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var userRoutes = __importStar(require("./src/features/user/index"));
var servicesRoutes = __importStar(require("./src/features/medicalServices"));
var doctorRoutes = __importStar(require("./src/features/authors/index"));
var orderRoutes = __importStar(require("./src/features/orders/index"));
var cookieParser = __importStar(require("cookie-parser"));
var config_1 = require("./src/config");
var port = config_1.config.port;
var app = express_1.default.application = express_1.default();
app.listen(port, function () {
    console.log('Server is up and running on port number ' + port);
});
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: false }));
app.use(cookieParser.default());
//const router = express.Router();
// router.use('/user/services', ServicesRoutes.userRouter); //todo like in example
// router.use('/admin/services', ServicesRoutes.adminRouter);
app.use('/user/services', servicesRoutes.userRouter);
app.use('/admin/services', servicesRoutes.adminRouter);
app.use('', userRoutes.userRouter);
app.use('/admin/users', userRoutes.adminRouter);
app.use('/user/doctors', doctorRoutes.userRouter);
app.use('/admin/doctors', doctorRoutes.adminRouter);
app.use('/user/orders', orderRoutes.userRouter);
app.use('/admin/orders', orderRoutes.adminRouter);
module.exports = app;
//# sourceMappingURL=index.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: 'c:/Users/Anuitex-130/BookStore2/backend/.env' });
exports.config = {
    dbConnectionString: process.env.dbConnectionString,
    dbName: process.env.dbName,
    port: process.env.port,
    jwtSecret: process.env.jwtSecret,
    accessToken: process.env.accessToken,
    refreshToken: process.env.refreshToken,
    expiresAccess: process.env.expiresAccess,
    expiresRefresh: process.env.expiresRefresh,
};
//# sourceMappingURL=config.js.map
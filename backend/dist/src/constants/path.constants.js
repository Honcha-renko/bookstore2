"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var signUpPath = '/signUp';
var getFilteredPath = '/getFiltered';
var updatePath = '/update';
exports.constants = {
    signUp: signUpPath,
    getFiltered: getFilteredPath,
    update: updatePath,
};
//# sourceMappingURL=path.constants.js.map
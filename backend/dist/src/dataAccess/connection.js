"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = require("../config");
var mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.set('debug', true);
var url = config_1.config.dbConnectionString;
mongoose_1.default.Promise = global.Promise;
mongoose_1.default.connect(url, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, });
var db = mongoose_1.default.connection;
db.on("error", console.error.bind(console, "MongoDB Connection error"));
exports.default = mongoose_1.default;
//# sourceMappingURL=connection.js.map
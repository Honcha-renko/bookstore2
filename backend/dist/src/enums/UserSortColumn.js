"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UserSortColumn;
(function (UserSortColumn) {
    UserSortColumn[UserSortColumn["FirstName"] = 1] = "FirstName";
    UserSortColumn[UserSortColumn["LastName"] = 2] = "LastName";
    UserSortColumn[UserSortColumn["Email"] = 3] = "Email";
})(UserSortColumn = exports.UserSortColumn || (exports.UserSortColumn = {}));
//# sourceMappingURL=UserSortColumn.js.map
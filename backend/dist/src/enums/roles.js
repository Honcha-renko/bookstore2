"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Roles;
(function (Roles) {
    Roles[Roles["user"] = 1] = "user";
    Roles[Roles["admin"] = 2] = "admin";
})(Roles = exports.Roles || (exports.Roles = {}));
;
//# sourceMappingURL=roles.js.map
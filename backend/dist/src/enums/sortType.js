"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SortType;
(function (SortType) {
    SortType[SortType["None"] = 0] = "None";
    SortType[SortType["Asc"] = 1] = "Asc";
    SortType[SortType["Desc"] = 2] = "Desc";
})(SortType = exports.SortType || (exports.SortType = {}));
//# sourceMappingURL=sortType.js.map
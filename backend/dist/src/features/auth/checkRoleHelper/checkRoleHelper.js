"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function checkRole(userRole, roles) {
    var result = roles.find(function (role) { return role == userRole; });
    if (!result) {
        return false;
    }
    return true;
}
exports.checkRole = checkRole;
//# sourceMappingURL=checkRoleHelper.js.map
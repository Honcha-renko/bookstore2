"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsonwebtoken_1 = __importStar(require("jsonwebtoken"));
var uuid_1 = require("uuid");
var config_1 = require("../../../config");
var jwtToken_1 = __importDefault(require("../../shared/jwt/models/jwtToken"));
var user_repository_1 = require("../../user/repositories/user.repository");
function createTokens(user) {
    var accessData = {
        id: user.id,
        role: user.role,
        type: config_1.config.accessToken
    };
    var refreshData = {
        tokenId: uuid_1.v4(),
        type: config_1.config.refreshToken,
    };
    var accessToken = jsonwebtoken_1.default.sign(accessData, config_1.config.jwtSecret, { expiresIn: config_1.config.expiresAccess });
    var refreshToken = jsonwebtoken_1.default.sign(refreshData, config_1.config.jwtSecret, { expiresIn: config_1.config.expiresRefresh });
    return { accessToken: accessToken, refreshToken: refreshToken, tokenId: refreshData.tokenId };
}
exports.createTokens = createTokens;
;
function replaceToken(tokenId, userId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, jwtToken_1.default.deleteOne({ userId: userId })];
                case 1:
                    _a.sent();
                    return [4 /*yield*/, jwtToken_1.default.create({ tokenId: tokenId, userId: userId })];
                case 2:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
exports.replaceToken = replaceToken;
function refreshToken(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var tokens, payload, token, user, newTokens;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    tokens = req.body;
                    try {
                        payload = jsonwebtoken_1.default.verify(tokens.refreshToken, config_1.config.jwtSecret);
                        if (payload.type != 'refresh') {
                            res.status(400).json({ message: 'Invalid token' });
                            return [2 /*return*/];
                        }
                    }
                    catch (error) {
                        if (error instanceof jsonwebtoken_1.JsonWebTokenError || error instanceof jsonwebtoken_1.TokenExpiredError) {
                            res.status(400).json({ message: 'Invalid token.' });
                            return [2 /*return*/];
                        }
                    }
                    return [4 /*yield*/, jwtToken_1.default.findOne({ tokenId: payload.tokenId })];
                case 1:
                    token = _a.sent();
                    if (token == null) {
                        throw new Error('Invalid token');
                    }
                    return [4 /*yield*/, user_repository_1.findById({ _id: token.userId })];
                case 2:
                    user = _a.sent();
                    newTokens = createTokens(user);
                    return [4 /*yield*/, replaceToken(payload.tokenId, user.id)];
                case 3:
                    _a.sent();
                    res.send(newTokens);
                    return [2 /*return*/];
            }
        });
    });
}
exports.refreshToken = refreshToken;
;
function authMiddleware(req, res, next) {
    var header = req.get('Authorization');
    if (!header) {
        res.status(401).json({
            error: 'Token is not provided'
        });
        return;
    }
    var token = header.replace('Bearer', '').trimLeft();
    try {
        var payload = jsonwebtoken_1.default.verify(token, config_1.config.jwtSecret);
        if (payload.type != 'access') {
            res.status(401).json({ message: 'Invalid token' });
            return;
        }
    }
    catch (error) {
        if (error instanceof jsonwebtoken_1.default.JsonWebTokenError) {
            res.status(401).json({ error: 'token is not valid' });
            return;
        }
        if (error instanceof jsonwebtoken_1.default.TokenExpiredError) {
            res.status(401).json({ message: 'Token expired.' });
            return;
        }
    }
    next();
}
exports.authMiddleware = authMiddleware;
;
function signOut(userId) {
    return __awaiter(this, void 0, void 0, function () {
        var result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, jwtToken_1.default.deleteOne({ userId: userId })];
                case 1:
                    result = _a.sent();
                    return [2 /*return*/, result];
            }
        });
    });
}
exports.signOut = signOut;
module.exports = {
    createTokens: createTokens,
    replaceToken: replaceToken,
    refreshToken: refreshToken,
    authMiddleware: authMiddleware,
    signOut: signOut,
};
//# sourceMappingURL=jwtHelper.js.map
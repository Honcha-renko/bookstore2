"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var userHandler = __importStar(require("../authors/handlers/author.user.handler"));
exports.userRouter = express_1.default.Router();
exports.userRouter.get('/getAll', userHandler.getPrintingEditions);
// userRouter.get('/get', userHandler.getById);
exports.adminRouter = express_1.default.Router();
// adminRouter.get('/getAll', adminHandler.getfilteredData);
// adminRouter.post('/create', adminHandler.create);
// adminRouter.post('/update', adminHandler.update);
module.exports = {
    userRouter: exports.userRouter,
    adminRouter: exports.adminRouter,
};
//# sourceMappingURL=index.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var userHandler = __importStar(require("./handlers/medicalServices.user.handler"));
var adminHandler = __importStar(require("./handlers/medicalServices.admin.handler"));
exports.userRouter = express_1.default.Router();
exports.userRouter
    .get('/getFiltered', userHandler.getFilteredData) //todo "?search&limit&offset" -->is this neccesary
    .get(':id/cover') //todo ???
    .get('/:id', userHandler.getById);
exports.adminRouter = express_1.default.Router();
exports.adminRouter
    .get('/getFiltered', adminHandler.getfilteredDataAsync)
    .get(':id', adminHandler.getById)
    .post('', adminHandler.create)
    .put(':id', adminHandler.update)
    .delete(':id', adminHandler.markAsRemoved);
//# sourceMappingURL=index.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var userHandler = __importStar(require("../orders/handlers/order.user.handler"));
var adminHandler = __importStar(require("../orders/handlers/order.admin.handler"));
//import * as doctorHandler from '../orders/handlers/order.admin.handler';//is this neccesary?
exports.userRouter = express_1.default.Router();
exports.userRouter
    .get(':id')
    .post('', userHandler.create);
exports.adminRouter = express_1.default.Router();
exports.adminRouter
    .get('/getFiltered', adminHandler.getFilteredDataAsync)
    .get(':id', userHandler.getById)
    .delete(':id', adminHandler.markAsRemoved);
//# sourceMappingURL=index.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var userHandler = __importStar(require("../printing-editions/handlers/printing-edition.user.handler"));
var adminHandler = __importStar(require("../printing-editions/handlers/printing-edition.admin.handler"));
exports.userRouter = express_1.default.Router();
exports.userRouter.get('/getAll', userHandler.getPrintingEditions);
exports.userRouter.get('/get', userHandler.getById);
exports.adminRouter = express_1.default.Router();
exports.adminRouter.get('/getAll', adminHandler.getfilteredData);
exports.adminRouter.post('/create', adminHandler.create);
exports.adminRouter.post('/update', adminHandler.update);
module.exports = {
    userRouter: exports.userRouter,
    adminRouter: exports.adminRouter,
};
//# sourceMappingURL=index.js.map
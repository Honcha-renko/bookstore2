"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ServiceSortColumn;
(function (ServiceSortColumn) {
    ServiceSortColumn[ServiceSortColumn["name"] = 1] = "name";
    ServiceSortColumn[ServiceSortColumn["serviceType"] = 2] = "serviceType";
    ServiceSortColumn[ServiceSortColumn["price"] = 3] = "price";
})(ServiceSortColumn = exports.ServiceSortColumn || (exports.ServiceSortColumn = {}));
//# sourceMappingURL=ServiceSortColumn.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UserSortColumn;
(function (UserSortColumn) {
    UserSortColumn[UserSortColumn["firstName"] = 1] = "firstName";
    UserSortColumn[UserSortColumn["lastName"] = 2] = "lastName";
    UserSortColumn[UserSortColumn["email"] = 3] = "email";
})(UserSortColumn = exports.UserSortColumn || (exports.UserSortColumn = {}));
//# sourceMappingURL=UserSortColumn.js.map
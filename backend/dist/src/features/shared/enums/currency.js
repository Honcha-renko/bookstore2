"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Currency;
(function (Currency) {
    Currency[Currency["USD"] = 0] = "USD";
    Currency[Currency["EUR"] = 1] = "EUR";
    Currency[Currency["JPY"] = 2] = "JPY";
    Currency[Currency["GBP"] = 3] = "GBP";
})(Currency = exports.Currency || (exports.Currency = {}));
;
//# sourceMappingURL=currency.js.map
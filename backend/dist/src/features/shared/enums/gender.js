"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Gender;
(function (Gender) {
    Gender[Gender["male"] = 1] = "male";
    Gender[Gender["female"] = 2] = "female";
    Gender[Gender["other"] = 3] = "other";
})(Gender = exports.Gender || (exports.Gender = {}));
//# sourceMappingURL=gender.js.map
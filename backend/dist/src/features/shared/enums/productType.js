"use strict";
var ProductType;
(function (ProductType) {
    ProductType[ProductType["None"] = 0] = "None";
    ProductType[ProductType["Book"] = 1] = "Book";
    ProductType[ProductType["Journal"] = 2] = "Journal";
    ProductType[ProductType["Newspaper"] = 3] = "Newspaper";
})(ProductType || (ProductType = {}));
;
exports = ProductType;
//# sourceMappingURL=productType.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Qualification;
(function (Qualification) {
    Qualification[Qualification["firstQualificationCategory"] = 1] = "firstQualificationCategory";
    Qualification[Qualification["secondQualificationCategory"] = 2] = "secondQualificationCategory";
    Qualification[Qualification["highestQualificationCategory"] = 3] = "highestQualificationCategory";
})(Qualification = exports.Qualification || (exports.Qualification = {}));
//# sourceMappingURL=qualification.js.map
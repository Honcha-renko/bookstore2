"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ServiceType;
(function (ServiceType) {
    ServiceType[ServiceType["None"] = 0] = "None";
    ServiceType[ServiceType["FirstType"] = 1] = "FirstType";
    ServiceType[ServiceType["SecondType"] = 2] = "SecondType";
    ServiceType[ServiceType["ThirdType"] = 3] = "ThirdType";
})(ServiceType = exports.ServiceType || (exports.ServiceType = {}));
;
//# sourceMappingURL=serviceType.js.map
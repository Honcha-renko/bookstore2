"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SortType;
(function (SortType) {
    SortType[SortType["Asc"] = 1] = "Asc";
    SortType[SortType["Desc"] = -1] = "Desc";
})(SortType = exports.SortType || (exports.SortType = {}));
//# sourceMappingURL=sortType.js.map
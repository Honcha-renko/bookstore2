"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Specialization;
(function (Specialization) {
    Specialization[Specialization["surgery"] = 1] = "surgery";
    Specialization[Specialization["dermatology"] = 2] = "dermatology";
    Specialization[Specialization["anesthesiology"] = 3] = "anesthesiology";
    Specialization[Specialization["Neurology"] = 4] = "Neurology";
})(Specialization = exports.Specialization || (exports.Specialization = {}));
//# sourceMappingURL=specialization.js.map
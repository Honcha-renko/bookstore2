"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Time;
(function (Time) {
    Time[Time["none"] = 0] = "none";
    Time[Time["9-10"] = 1] = "9-10";
    Time[Time["10-11"] = 2] = "10-11";
    Time[Time["11-12"] = 3] = "11-12";
})(Time = exports.Time || (exports.Time = {}));
//# sourceMappingURL=timetable.js.map
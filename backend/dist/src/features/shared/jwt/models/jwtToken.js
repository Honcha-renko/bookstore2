"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var connection_1 = __importDefault(require("../../../../dataAccess/connection"));
var TokenSchema = new connection_1.default.Schema({
    tokenId: String,
    userId: String,
});
exports.default = connection_1.default.model('token', TokenSchema);
//# sourceMappingURL=jwtToken.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var connection_1 = __importDefault(require("../../../dataAccess/connection"));
var Schema = connection_1.default.Schema;
var authorSchema = new Schema({
    name: String,
}, { collection: 'authors' });
var authorModel = connection_1.default.model('Author', authorSchema);
exports.default = authorModel;
//# sourceMappingURL=authors.js.map
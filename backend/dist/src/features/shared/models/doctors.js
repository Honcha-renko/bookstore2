"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var connection_1 = __importDefault(require("../../../dataAccess/connection"));
var Schema = connection_1.default.Schema;
var doctorSchema = new Schema({
    name: String,
}, { collection: 'doctors' });
var doctorModel = connection_1.default.model('Doctor', doctorSchema);
exports.default = doctorModel;
//# sourceMappingURL=doctors.js.map
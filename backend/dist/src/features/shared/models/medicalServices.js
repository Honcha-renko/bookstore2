"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var connection_1 = __importDefault(require("../../../dataAccess/connection"));
var serviceType_1 = require("../enums/serviceType");
var currency_1 = require("../enums/currency");
var Schema = connection_1.default.Schema;
var ServicesSchema = new Schema({
    name: { type: String, required: true, unique: true },
    type: { type: serviceType_1.ServiceType, required: true },
    description: { type: String, required: false, unique: false },
    doctor_Ids: [{ type: connection_1.default.Schema.Types.ObjectId, ref: 'User', required: true }],
    price: { type: Number, required: true },
    currency: { type: currency_1.Currency, required: true },
    addInfo: { type: String, required: false },
    coverImage: { type: String, required: false },
    removedAt: { type: Boolean, required: true },
}, { collection: 'services' });
exports.default = connection_1.default.model('MedicalServices', ServicesSchema);
//# sourceMappingURL=medicalServices.js.map
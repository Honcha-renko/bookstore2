"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var connection_1 = __importDefault(require("../../../dataAccess/connection"));
var currency_1 = require("../enums/currency");
var Schema = connection_1.default.Schema;
var orderSchema = new Schema({
    userId: { type: connection_1.default.Schema.Types.ObjectId, ref: 'User', required: true },
    paymentId: { type: connection_1.default.Schema.Types.ObjectId, ref: 'Payment', required: true },
    status: { type: Boolean, required: true },
    items: [{
            serviceId: { type: connection_1.default.Schema.Types.ObjectId, ref: 'Service', required: true },
            doctorId: { type: connection_1.default.Schema.Types.ObjectId, ref: 'Doctor', required: true },
            timetableId: { type: connection_1.default.Schema.Types.ObjectId, ref: 'Timetable', required: true },
            count: { type: Number, required: true },
            currency: { type: currency_1.Currency, required: true },
            addInfo: { type: String, required: false },
            amount: { type: Number, required: true },
        }]
}, { collection: 'orders' });
exports.orderModel = connection_1.default.model('Order', orderSchema);
//# sourceMappingURL=orders.js.map
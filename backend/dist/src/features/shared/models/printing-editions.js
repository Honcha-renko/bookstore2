"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var connection_1 = __importDefault(require("../../../dataAccess/connection"));
var Schema = connection_1.default.Schema;
var categories = require('../../../enums/ProductType');
var printingEditionSchema = new Schema({
    name: String,
    description: String,
    coverImage: String,
    removedAt: Boolean,
    type: Enumerator,
    lastName: String,
    passwordHash: String,
    //categories //enum?    
    versionKey: Boolean
}, { collection: 'users' });
var printingEditionModel = connection_1.default.model('User', printingEditionSchema);
module.exports = {
    User: printingEditionModel
};
//# sourceMappingURL=printing-editions.js.map
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var connection_1 = __importDefault(require("../../../dataAccess/connection"));
var Schema = connection_1.default.Schema;
var registrationSchema = new Schema({
    email: { type: String, required: true, unique: true },
    firstName: { type: String, required: true, unique: false },
    lastName: { type: String, required: true, unique: false },
    password: { type: String, required: true, unique: true },
});
var registrationModel = connection_1.default.model('Registration', registrationSchema);
exports.default = registrationModel;
//# sourceMappingURL=registration.schema.js.map
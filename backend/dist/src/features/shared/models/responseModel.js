"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var connection_1 = __importDefault(require("../../../dataAccess/connection"));
var Schema = connection_1.default.Schema;
var ResponseModelSchema = new Schema({
    data: { type: Array, required: true },
    totalCount: { type: Number, required: false },
});
var responseModel = connection_1.default.model('responseModel', ResponseModelSchema);
exports.default = responseModel;
//# sourceMappingURL=responseModel.js.map
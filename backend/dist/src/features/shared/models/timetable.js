"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connection_1 = __importDefault(require("../../../dataAccess/connection"));
var Schema = connection_1.default.Schema;
var timetable_1 = require("../enums/timetable");
var timetableSchema = new Schema({
    date: { type: String, required: true },
    time: { type: timetable_1.Time, required: true },
    taken: { type: Boolean, required: true },
}, { collection: 'timetable' });
var timetableModel = connection_1.default.model('Timetable', timetableSchema);
module.exports = timetableModel;
//# sourceMappingURL=timetable.js.map
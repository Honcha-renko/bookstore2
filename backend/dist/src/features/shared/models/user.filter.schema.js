"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var connection_1 = __importDefault(require("../../../dataAccess/connection"));
var Schema = connection_1.default.Schema;
var sortType_1 = require("../enums/sortType");
var userStatus_1 = require("../enums/userStatus");
var UserSortColumn_1 = require("../enums/UserSortColumn");
var userFilterSchema = new Schema({
    pageNumber: { type: Number, required: true },
    pageSize: { type: Number, required: true },
    searchText: { type: String, required: false },
    sortType: { type: sortType_1.SortType, required: false },
    userStatus: { type: userStatus_1.UserStatus, required: true },
    userSorting: { type: UserSortColumn_1.UserSortColumn, required: true },
});
var userFilter = connection_1.default.model('userFilter', userFilterSchema);
exports.default = userFilter;
//# sourceMappingURL=user.filter.schema.js.map
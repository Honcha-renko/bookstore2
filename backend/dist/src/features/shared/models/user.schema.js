"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connection_1 = __importDefault(require("../../../dataAccess/connection"));
var Schema = connection_1.default.Schema;
var roles_1 = require("../enums/roles");
var specialization_1 = require("../enums/specialization");
var userSchema = new Schema({
    email: { type: String, required: true, unique: true },
    passwordHash: { type: String, required: true, unique: true },
    avatar: { type: String, required: false, unique: false },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    role: { type: roles_1.Roles, required: true },
    adress: { type: String, required: true, unique: false },
    specialization: { type: specialization_1.Specialization, unique: false },
    timetableId: { type: connection_1.default.Schema.Types.ObjectId, ref: 'Timetable' },
}, { collection: 'users' });
var userModel = connection_1.default.model('User', userSchema);
module.exports = userModel;
//# sourceMappingURL=user.schema.js.map
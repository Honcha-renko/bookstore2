"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = require("../../../config");
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
function getPayload(req) {
    var token = req.get('Authorization').replace('Bearer', '').trimLeft();
    var payload = jsonwebtoken_1.default.verify(token, config_1.config.jwtSecret);
    return payload;
}
exports.getPayload = getPayload;
//# sourceMappingURL=getPayload.js.map
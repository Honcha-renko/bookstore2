"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Token = __importStar(require("../../auth/jwtHelper/jwtHelper"));
var userService = __importStar(require("../services/user.service"));
var config_1 = require("../../../config");
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
function signUpAsync(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var resultModel, user, tokenData;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, userService.signUpAsync(req.body)];
                case 1:
                    resultModel = _a.sent();
                    if (!(resultModel === true)) return [3 /*break*/, 3];
                    return [4 /*yield*/, userService.findByEmail(req.body.email)];
                case 2:
                    user = _a.sent();
                    tokenData = Token.createTokens(user);
                    res.send(tokenData);
                    _a.label = 3;
                case 3:
                    res.send(resultModel);
                    return [2 /*return*/];
            }
        });
    });
}
exports.signUpAsync = signUpAsync;
;
function signIn(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var result, user, tokenData, replaceToken;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, userService.signInAsync(req.body)];
                case 1:
                    result = _a.sent();
                    if (!result) return [3 /*break*/, 4];
                    return [4 /*yield*/, userService.findByEmail(req.body.email)];
                case 2:
                    user = _a.sent();
                    tokenData = Token.createTokens(user);
                    return [4 /*yield*/, Token.replaceToken(tokenData.tokenId, user.id)];
                case 3:
                    replaceToken = _a.sent();
                    res.setHeader('accessToken', tokenData.accessToken);
                    res.setHeader('refreshToken', tokenData.refreshToken);
                    return [2 /*return*/, res.send(tokenData)];
                case 4: return [2 /*return*/, res.send(result)];
            }
        });
    });
}
exports.signIn = signIn;
function update(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var header, token, payload, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    header = req.get('Authorization');
                    token = header.replace('Bearer', '').trimLeft();
                    payload = jsonwebtoken_1.default.verify(token, config_1.config.jwtSecret);
                    return [4 /*yield*/, userService.update(payload, req.body)];
                case 1:
                    result = _a.sent();
                    return [2 /*return*/, res.send(result)];
            }
        });
    });
}
exports.update = update;
function signOut(req, res) {
    return __awaiter(this, void 0, void 0, function () {
        var result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, userService.signOut(req.body._id)];
                case 1:
                    result = _a.sent();
                    return [2 /*return*/, res.send(result)];
            }
        });
    });
}
exports.signOut = signOut;
//# sourceMappingURL=user.user.handler.js.map
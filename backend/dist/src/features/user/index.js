"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var userHandler = __importStar(require("./handlers/user.user.handler"));
var adminHandler = __importStar(require("./handlers/user.admin.handler"));
var authMiddleware = __importStar(require("../auth/jwtHelper/jwtHelper"));
exports.userRouter = express_1.default.Router();
exports.userRouter
    .post('/signUp', userHandler.signUpAsync)
    .post('/me', userHandler.signIn)
    .post('/update', authMiddleware.authMiddleware, userHandler.update)
    .post('/logOut', authMiddleware.authMiddleware, userHandler.signOut)
    .post('/refreshTokens', authMiddleware.refreshToken);
exports.adminRouter = express_1.default.Router();
exports.adminRouter
    .get('/getFilteredData', authMiddleware.authMiddleware, adminHandler.getFilteredDataAsync);
//# sourceMappingURL=index.js.map
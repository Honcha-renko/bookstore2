"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var registrationRequest_schema_json_1 = __importDefault(require("./registrationRequest.schema.json"));
var updateValidation_schema_json_1 = __importDefault(require("./updateValidation.schema.json"));
var getFilteredUsersValidation_Schema_json_1 = __importDefault(require("./getFilteredUsersValidation.Schema.json"));
var signInValidationSchema_json_1 = __importDefault(require("./signInValidationSchema.json"));
module.exports = {
    registrationSchema: registrationRequest_schema_json_1.default,
    updateSchema: updateValidation_schema_json_1.default,
    getFilteredSchema: getFilteredUsersValidation_Schema_json_1.default,
    signInSchema: signInValidationSchema_json_1.default,
};
//# sourceMappingURL=validationSchemas.js.map
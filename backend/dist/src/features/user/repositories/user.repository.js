"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var bcrypt_1 = __importDefault(require("bcrypt"));
var roles_1 = require("../../shared/enums/roles");
var UserSortColumn_1 = require("../../shared/enums/UserSortColumn");
var user = __importStar(require("../../shared/models/user.schema"));
var User = user.default;
function getFilteredDataAsync(filter) {
    return __awaiter(this, void 0, void 0, function () {
        var property, sort, users, totalCount, data;
        var _a;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    property = UserSortColumn_1.UserSortColumn[filter.userSortColumn];
                    sort = (_a = {}, _a[property] = filter.sortType, _a);
                    users = User.find({ role: roles_1.Roles.user });
                    if (!filter.searchText.startsWith(' ') && filter.searchText.length != 0) {
                        users = users.find({ firstName: filter.searchText } || { lastName: filter.searchText } || { email: filter.searchText });
                    }
                    users.sort(sort);
                    return [4 /*yield*/, users];
                case 1:
                    totalCount = (_b.sent()).length;
                    users.skip((filter.pageNumber - 1) * filter.pageSize);
                    return [4 /*yield*/, users.limit(filter.pageSize)];
                case 2:
                    data = _b.sent();
                    return [2 /*return*/, { data: data, totalCount: totalCount }];
            }
        });
    });
}
exports.getFilteredDataAsync = getFilteredDataAsync;
;
function findByEmail(email) {
    return __awaiter(this, void 0, void 0, function () {
        var user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, User.findOne({ email: email })];
                case 1:
                    user = _a.sent();
                    return [2 /*return*/, user];
            }
        });
    });
}
exports.findByEmail = findByEmail;
function findById(id) {
    return __awaiter(this, void 0, void 0, function () {
        var user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, User.findById(id)];
                case 1:
                    user = _a.sent();
                    return [2 /*return*/, user];
            }
        });
    });
}
exports.findById = findById;
function update(user) {
    return __awaiter(this, void 0, void 0, function () {
        var result, _a, _b, _c, _d;
        return __generator(this, function (_e) {
            switch (_e.label) {
                case 0: return [4 /*yield*/, User.updateOne({ _id: user._id }, { '$set': user })];
                case 1:
                    result = _e.sent();
                    if (!user.changePasswordBox) return [3 /*break*/, 4];
                    _b = (_a = User).updateOne;
                    _c = [{ _id: user._id }];
                    _d = {};
                    return [4 /*yield*/, bcrypt_1.default.hash(user.password, 10)];
                case 2: return [4 /*yield*/, _b.apply(_a, _c.concat([(_d.passwordHash = _e.sent(), _d)]))];
                case 3:
                    result = _e.sent();
                    _e.label = 4;
                case 4: return [2 /*return*/, result];
            }
        });
    });
}
exports.update = update;
//# sourceMappingURL=user.repository.js.map
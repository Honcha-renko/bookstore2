"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var bcrypt_1 = __importDefault(require("bcrypt"));
var checkRoleHelper_1 = require("../../auth/checkRoleHelper/checkRoleHelper");
var authMiddleware = __importStar(require("../../auth/jwtHelper/jwtHelper"));
var registration_1 = __importDefault(require("../../auth/registration/registration"));
var signIn_1 = require("../../auth/signIn/signIn");
var roles_1 = require("../../shared/enums/roles");
var logger_1 = __importDefault(require("../../utils/logger"));
var validateWithSchema_1 = require("../../utils/validateWithSchema");
var validationSchemas_1 = __importDefault(require("../operations/validationSchemas"));
var userRepository = __importStar(require("../repositories/user.repository"));
var userModel = __importStar(require("../../shared/models/user.schema"));
var User = userModel.default;
function signUpAsync(registrationData) {
    return __awaiter(this, void 0, void 0, function () {
        var validationResult, user, _a, _b, result;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    logger_1.default.log('info', ">>>>userService.signUp(), with: user = " + JSON.stringify(registrationData));
                    validationResult = validateWithSchema_1.validateWithSchema(registrationData, validationSchemas_1.default.registrationSchema);
                    if (!validationResult.valid) {
                        logger_1.default.log('error', ">>>>userService.signUp(), invalid data: " + validationResult.errors);
                        return [2 /*return*/, { message: "Invalid request", errors: validationResult.errors }];
                    }
                    _a = User.bind;
                    _b = {
                        email: registrationData.email,
                        firstName: registrationData.firstName,
                        lastName: registrationData.lastName
                    };
                    return [4 /*yield*/, bcrypt_1.default.hash(registrationData.password, 10)];
                case 1:
                    user = new (_a.apply(User, [void 0, (_b.passwordHash = _c.sent(),
                            _b.adress = registrationData.adress,
                            _b.role = roles_1.Roles.user,
                            _b)]))();
                    return [4 /*yield*/, registration_1.default(user)];
                case 2:
                    result = _c.sent();
                    if (!result) {
                        logger_1.default.log('error', "<<<< userService.signUp(), result =  " + result);
                        return [2 /*return*/, "User with this email is already exist"];
                    }
                    ;
                    return [2 /*return*/, result];
            }
        });
    });
}
exports.signUpAsync = signUpAsync;
;
function getFilteredDataAsync(payload, filter) {
    return __awaiter(this, void 0, void 0, function () {
        var validationResult, result, data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', ">>>>userService.getFilteredDataAsync(), with: filter = " + JSON.stringify(filter));
                    validationResult = validateWithSchema_1.validateWithSchema(filter, validationSchemas_1.default.getFilteredSchema);
                    if (!validationResult.valid) {
                        logger_1.default.log('error', ">>>>userService.getFilteredDataAsync(), invalid data: " + validationResult.errors);
                        return [2 /*return*/, { message: "Invalid request", errors: validationResult.errors }];
                    }
                    result = checkRoleHelper_1.checkRole(payload.role, [1]);
                    if (!result) {
                        logger_1.default.log('error', "<<<< userService.getFilteredDataAsync(), result =  " + result);
                        return [2 /*return*/, "Access denied."];
                    }
                    return [4 /*yield*/, userRepository.getFilteredDataAsync(filter)];
                case 1:
                    data = _a.sent();
                    return [2 /*return*/, data];
            }
        });
    });
}
exports.getFilteredDataAsync = getFilteredDataAsync;
;
function signInAsync(signInModel) {
    return __awaiter(this, void 0, void 0, function () {
        var validationResult, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', ">>>>userService.signInAsync(), with: signInModel = " + JSON.stringify(signInModel));
                    validationResult = validateWithSchema_1.validateWithSchema(signInModel, validationSchemas_1.default.signInSchema);
                    if (!validationResult.valid) {
                        logger_1.default.log('error', ">>>>userService.signInAsync(), invalid data: " + validationResult.errors);
                        return [2 /*return*/, { message: "Invalid request", errors: validationResult.errors }];
                    }
                    return [4 /*yield*/, signIn_1.signIn(signInModel)];
                case 1:
                    result = _a.sent();
                    if (!result) {
                        logger_1.default.log('error', "<<<< signIn(), result =  " + result);
                    }
                    return [2 /*return*/, result];
            }
        });
    });
}
exports.signInAsync = signInAsync;
;
function findByEmail(email) {
    return __awaiter(this, void 0, void 0, function () {
        var user;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, userRepository.findByEmail(email)];
                case 1:
                    user = _a.sent();
                    return [2 /*return*/, user];
            }
        });
    });
}
exports.findByEmail = findByEmail;
;
function update(payload, user) {
    return __awaiter(this, void 0, void 0, function () {
        var validationResult, checkResult, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    logger_1.default.log('info', ">>>>userService.update(), with: user = " + JSON.stringify(user));
                    validationResult = validateWithSchema_1.validateWithSchema(user, validationSchemas_1.default.updateSchema);
                    if (!validationResult.valid) {
                        logger_1.default.log('error', ">>>>userService.update(), invalid data: " + validationResult.errors);
                        return [2 /*return*/, { message: "Invalid request", errors: validationResult.errors }];
                    }
                    checkResult = checkRoleHelper_1.checkRole(payload.role, [1, 2, 3]);
                    if (checkResult) {
                        logger_1.default.log('error', "<<<< userService.update(), result =  " + checkResult);
                        return [2 /*return*/, "Access denied."];
                    }
                    return [4 /*yield*/, userRepository.update(user)];
                case 1:
                    result = _a.sent();
                    if (result.errors) {
                        logger_1.default.log('error', "update(), result = " + JSON.stringify(result));
                    }
                    return [2 /*return*/, result];
            }
        });
    });
}
exports.update = update;
function signOut(userId) {
    return __awaiter(this, void 0, void 0, function () {
        var result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, authMiddleware.signOut(userId)];
                case 1:
                    result = _a.sent();
                    if (result.deletedCount == 0) {
                        return [2 /*return*/, false];
                    }
                    return [2 /*return*/, true];
            }
        });
    });
}
exports.signOut = signOut;
//# sourceMappingURL=user.service.js.map
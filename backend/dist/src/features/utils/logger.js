"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var winston_1 = __importDefault(require("winston"));
var format = winston_1.default.format;
var logger = winston_1.default.createLogger({
    format: winston_1.default.format.combine(format.timestamp(), format.errors({ stack: true }), format.json(), format.prettyPrint()),
    defaultMeta: { service: 'user-service' },
    transports: [
        new winston_1.default.transports.File({ level: 'info', filename: 'info.log', }),
        new winston_1.default.transports.File({ level: 'error', filename: 'error.log', }),
    ],
    exitOnError: false,
});
if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston_1.default.transports.Console({
        handleExceptions: true,
        format: format.combine(format.colorize(), format.simple())
    }));
}
exports.default = logger;
//# sourceMappingURL=logger.js.map
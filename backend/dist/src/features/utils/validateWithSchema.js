"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var jsonschema_1 = require("jsonschema");
function validateWithSchema(value, schema) {
    return jsonschema_1.validate(value, schema);
}
exports.validateWithSchema = validateWithSchema;
//# sourceMappingURL=validateWithSchema.js.map
import express from "express";
import bodyParser from "body-parser";
import * as userRoutes from "./src/features/user/index";
import * as servicesRoutes from './src/features/medicalServices';
import *  as doctorRoutes from './src/features/authors/index';
import * as orderRoutes from './src/features/orders/index';
import * as cookieParser from 'cookie-parser';
import { config } from "./src/config";
const port = config.port;

let app = express.application = express();

app.listen(port, () => {
    console.log('Server is up and running on port number ' + port);
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser.default());

//const router = express.Router();
// router.use('/user/services', ServicesRoutes.userRouter); //todo like in example
// router.use('/admin/services', ServicesRoutes.adminRouter);

app.use('/user/services', servicesRoutes.userRouter);
app.use('/admin/services', servicesRoutes.adminRouter);

app.use('', userRoutes.userRouter);
app.use('/admin/users', userRoutes.adminRouter);

app.use('/user/doctors', doctorRoutes.userRouter);
app.use('/admin/doctors', doctorRoutes.adminRouter);

app.use('/user/orders', orderRoutes.userRouter);
app.use('/admin/orders', orderRoutes.adminRouter);

// app.use('/user/medcard', medcardRoutes.userRouter);
// app.use('/doctor/medcard', medcardRoutes.adminRouter);

export = app; 
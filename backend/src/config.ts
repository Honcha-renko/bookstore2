import dotenv from 'dotenv';
dotenv.config({ path: 'c:/Users/Anuitex-130/BookStore2/backend/.env' });

export const config = {
    dbConnectionString: process.env.dbConnectionString as string,
    dbName: process.env.dbName as string,
    port: process.env.port,
    jwtSecret: process.env.jwtSecret as string,
    accessToken: process.env.accessToken as string,
    refreshToken: process.env.refreshToken as string,
    expiresAccess: process.env.expiresAccess,
    expiresRefresh: process.env.expiresRefresh,

};
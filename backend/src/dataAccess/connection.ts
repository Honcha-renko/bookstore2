import {config} from "../config";
import mongoose from "mongoose";
mongoose.set('debug', true);
const url =config.dbConnectionString;

mongoose.Promise = global.Promise;
mongoose.connect(url,{ useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true,});
let db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB Connection error"));


export default mongoose;
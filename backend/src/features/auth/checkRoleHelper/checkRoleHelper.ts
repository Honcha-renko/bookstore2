import { Roles } from "../../shared/enums/roles";

export function checkRole(userRole: Roles, roles: Array<Roles>) {
    const result = roles.find(role => role == userRole);
    if (!result) {
        return false;
    }
    return true;
}
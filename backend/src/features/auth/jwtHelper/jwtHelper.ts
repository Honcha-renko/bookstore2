import { NextFunction, Request, Response } from "express";
import jwt, { JsonWebTokenError, TokenExpiredError } from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';
import { config } from "../../../config";
import mongoose from "../../../dataAccess/connection";
import { Payload } from "../../shared/jwt/interfaces/payload";
import jwtToken from "../../shared/jwt/models/jwtToken";
import { User } from "../../shared/models/interfaces/user";
import { findById } from '../../user/repositories/user.repository';


export function createTokens(user: User & mongoose.Document) {

    const accessData = {
        id: user.id,
        role: user.role,
        type: config.accessToken
    };
    const refreshData = {
        tokenId: uuidv4(),
        type: config.refreshToken,
    };

    const accessToken = jwt.sign(accessData, config.jwtSecret, { expiresIn: config.expiresAccess });
    const refreshToken = jwt.sign(refreshData, config.jwtSecret, { expiresIn: config.expiresRefresh });
    return { accessToken, refreshToken, tokenId: refreshData.tokenId }
};

export async function replaceToken(tokenId: string, userId: string) {
    await jwtToken.deleteOne({ userId: userId });
    await jwtToken.create({ tokenId, userId })
}

export async function refreshToken(req: Request, res: Response) {
    const tokens = req.body;
    let payload;
    try {
        payload = <Payload>jwt.verify(tokens.refreshToken, config.jwtSecret);
        if (payload.type != 'refresh') {
            res.status(400).json({ message: 'Invalid token' });
            return;
        }
    }
    catch (error) {
        if (error instanceof JsonWebTokenError || error instanceof TokenExpiredError) {
            res.status(400).json({ message: 'Invalid token.' });
            return;
        }
    }
    const token = await <any>jwtToken.findOne({ tokenId: payload.tokenId });
    if (token == null) {
        throw new Error('Invalid token')
    }

    const user = await findById({ _id: token.userId });
    const newTokens = createTokens(user);
    await replaceToken(payload.tokenId, user.id);

    res.send(newTokens);
};

export function authMiddleware(req: Request, res: Response, next: NextFunction) {
    const header = req.get('Authorization');

    if (!header) {
        res.status(401).json({
            error: 'Token is not provided'
        });
        return;
    }

    const token = header.replace('Bearer', '').trimLeft();

    try {
        const payload = <Payload>jwt.verify(token, config.jwtSecret);
        if (payload.type != 'access') {
            res.status(401).json({ message: 'Invalid token' });
            return;
        }
    }
    catch (error) {
        if (error instanceof jwt.JsonWebTokenError) {
            res.status(401).json({ error: 'token is not valid' });
            return;
        }
        if (error instanceof jwt.TokenExpiredError) {
            res.status(401).json({ message: 'Token expired.' });
            return;
        }
    }
    next();
};

export async function signOut(userId: string) {
    const result = await jwtToken.deleteOne({ userId: userId });
    return result;
}

module.exports = {
    createTokens,
    replaceToken,
    refreshToken,
    authMiddleware,
    signOut,
}
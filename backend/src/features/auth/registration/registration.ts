import { User } from "../../shared/models/interfaces/user";
import * as user from '../../shared/models/user.schema';
const User = user.default;
import mongoose from "../../../dataAccess/connection";

export default async function signUpAsync(user: User & mongoose.Document): Promise<boolean> {
    const userCheck = await User.findOne({ email: user.email });
    if (userCheck !== null) {
        return false;
    }
    const result = await user.save();
    if (result.errors) {
        return false;
    }
    return true;
};

import bcrypt from 'bcrypt';
import User from '../../shared/models/user.schema';
import { SignIn } from '../../shared/models/interfaces/signIn';


export async function signIn(signInModel: SignIn): Promise<Boolean> {
    const user = await User.findOne({email:signInModel.email});

    if (!user) {
        return false;
    }

    const doPasswordsMatch = await bcrypt.compare(signInModel.password, user.passwordHash);
    return doPasswordsMatch;
};
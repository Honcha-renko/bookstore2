import express from "express";
import * as userHandler from '../authors/handlers/author.user.handler';
import * as adminHandler from '../authors/handlers/author.admin.handler';

export const userRouter = express.Router();

userRouter.get('/getAll', userHandler.getPrintingEditions);
// userRouter.get('/get', userHandler.getById);

export const adminRouter = express.Router();
// adminRouter.get('/getAll', adminHandler.getfilteredData);
// adminRouter.post('/create', adminHandler.create);
// adminRouter.post('/update', adminHandler.update);

module.exports = {
    userRouter,
    adminRouter,
};
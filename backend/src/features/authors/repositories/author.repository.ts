import  Doctor from '../../shared/models/interfaces/doctor';
import * as author from '../../shared/models/doctors';
const AuthorModel = author.default;
import { paginateData } from '../../shared/repositories/pagination';
import { UserFilter } from '../../shared/models/interfaces/filters/user.filter';
import { UserSortColumn } from '../../shared/enums/UserSortColumn';

export async function getFilteredDataAsync(filter: UserFilter): Promise<Object> {
    let authors = await AuthorModel.find().skip(1);

    if (!filter.searchText.startsWith(' ') && filter.searchText.length != 0) {
        authors = authors.filter(author => author.name == filter.searchText);
    }

    const property = UserSortColumn[filter.userSortColumn];//change
//    const sortType = SortType[filter.sortType];

    if (filter.sortType != 0) {//it is possible to make a chose if sorting
        authors.sort(function (a, b) {
            if (a.get(`${property}`) < b.get(`${property}`)) { return -1; }
            if (a.get(`${property}`) > b.get(`${property}`)) { return 1; }
            return 0;
        });
    }

    const paginatedData = paginateData(authors, filter);
    return paginatedData;
};

import { ObjectId } from "mongodb";

export interface PrintingEdition{
    _id: ObjectId|string;
    title:string;
}

export interface Author{
    _id: ObjectId|string;
    name:string;
}
import { Response, Request } from "express";
import * as medicalServicesServise from '../services/medicalServices.service';
import { getPayload } from "../../shared/repositories/getPayload";

export async function getfilteredDataAsync(req: Request, res: Response) {
    const payload = getPayload(req);
    const result = await medicalServicesServise.getFilteredDataAsync(payload, req.body);
    res.send(result);
}

export async function create(req: Request, res: Response) {
    //  const payload = getPayload(req);

    const result = await medicalServicesServise.create(req.body);
    res.send({ message: 'created successfully:', result });
}

export async function update(req: Request, res: Response) {
    const payload = getPayload(req);
}

export async function markAsRemoved(req: Request, res: Response) {
    const payload = getPayload(req);
}

export async function getById(req: Request, res: Response) {
    const payload = getPayload(req);
}
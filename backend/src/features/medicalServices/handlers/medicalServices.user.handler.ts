import { Request, Response } from "express";
import { getPayload } from "../../shared/repositories/getPayload";
import * as medicalServicesService from '../services/medicalServices.service';

export async function getFilteredData(req: Request, res: Response) {
  //  const payload = getPayload(req);
    const result = await medicalServicesService.getFilteredData( req.body);
    res.send(result);
}
export async function getById(req: Request, res: Response) {
    const result = await medicalServicesService.getById(req.params.id);//req.query.id
    return res.send(result);
}
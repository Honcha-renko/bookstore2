import express from "express";
import * as userHandler from './handlers/medicalServices.user.handler';
import * as adminHandler from './handlers/medicalServices.admin.handler';

export const userRouter = express.Router();
userRouter
    .get('/getFiltered', userHandler.getFilteredData)//todo "?search&limit&offset" -->is this neccesary
    .get(':id/cover')//todo ???
    .get('/:id', userHandler.getById);

export const adminRouter = express.Router();
adminRouter
    .get('/getFiltered', adminHandler.getfilteredDataAsync)
    .get(':id', adminHandler.getById)
    .post('', adminHandler.create)
    .put(':id', adminHandler.update)
    .delete(':id', adminHandler.markAsRemoved);
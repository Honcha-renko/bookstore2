import { ServiceFilter } from "../../shared/models/interfaces/filters/serviceFilter";
import mongoose from "../../../dataAccess/connection";
import { Roles } from "../../shared/enums/roles";
import { ServiceSortColumn } from "../../shared/enums/ServiceSortColumn";
import * as medicalServices from "../../shared/models/medicalServices";
const Service = medicalServices.default;
import { MedicalServices } from "../../shared/models/interfaces//medicalServices";
import { ObjectId } from "mongodb";
import userModel from "src/features/shared/models/user.schema";


// export function getPrintingEditionsForUser(/*params:type*/): PrintingEditionWithAuthors[] {

// } 

export async function getFilteredData(filter: ServiceFilter): Promise<Object> {
    const property = ServiceSortColumn[filter.serviceSortColumn];
    let sort = { [property]: filter.sortType };

    let services = Service.find().populate({ path: 'doctor_Ids', select: 'lastName' });//  .where({ roles: Roles.doctor })

    if (!filter.searchText.startsWith(' ') && filter.searchText.length != 0) {
        services = services.find({ name: { $in: [filter.searchText] } } || { description: { $in: [filter.searchText] } });//search by peaces of word
    }//fulltext search?

    // filter.serviceType.forEach(type => {
    //     services.find().where()
    // });

    //   if (filter.serviceType) {
    //         services = services.find({ name: filter.serviceType } || { description: filter.searchText });//todo find medServ by types 
    //     }

    // public IQueryable<PrintingEdition> SortByPrice(IQueryable<PrintingEdition> printingEditions, PrintingEditionFilterModel printingEditionFilterModel)
    // {
    //     printingEditions = printingEditions.Where(x => x.Price >= printingEditionFilterModel.MinPrice && x.Price <= printingEditionFilterModel.MaxPrice);
    //     return printingEditions;
    // }

    services.sort(sort);
    const totalCount = (await services).length
    services.skip((filter.pageNumber - 1) * filter.pageSize);
    const data = await services.limit(filter.pageSize);

    return { data, totalCount };
}

export async function create(service: MedicalServices & mongoose.Document): Promise<boolean> {
    const alreadyExists = await Service.findOne({ name: service.name });
    if (alreadyExists !== null) {
        return false;
    }

    const result = await service.save();
    if (result.errors) {
        return false;
    }
    return true;
}

export async function getById(id: ObjectId | string) {
    const service = await Service.findOne({ _id: id });
    if (!service) {
        return false;
    }
    if (service.errors != null) {
        return service.errors;
    }
    return service;
}
export function savePrintingEditions() {

} 
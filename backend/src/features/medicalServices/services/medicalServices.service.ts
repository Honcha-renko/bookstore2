import * as medicalServicesRepository from '../repositories/medicalServices.repository';
import { Payload } from '../../shared/jwt/interfaces/payload';
import { checkRole } from '../../auth/checkRoleHelper/checkRoleHelper';
import logger from '../../utils/logger';
import { ServiceFilter } from '../../shared/models/interfaces/filters/serviceFilter';
import mongoose from '../../../dataAccess/connection';
import { MedicalServices } from '../../shared/models/interfaces/medicalServices';
import * as serviceModel from '../../shared/models/medicalServices';
import { ObjectId } from 'mongodb';
let Services = serviceModel.default;

export function getFilteredDataAsync(payload: Payload, filter: ServiceFilter) {
    const checkResult = checkRole(payload.role, [1, 2, 3])
    if (checkResult) {
        logger.log('error', `>>>>userService.getFilteredData(), with: result = ${JSON.stringify(checkResult)}`)
    }
    const result = medicalServicesRepository.getFilteredData(filter);
    return result;
};

export async function getById(id: any) {
    const result = await medicalServicesRepository.getById(id);
    return result;
}

export async function create(/*payload: Payload,*/ service: MedicalServices) {
    const newService = new Services({
        name: service.name,
        type: service.type,
        description: service.description,
        doctor_Ids: service.doctor_Ids,
        price: service.price,
        currency: service.currency,
        addInfo: service.addInfo,
        coverImage: service.coverImage,
        removedAt: service.removedAt,
    });

    const result = await medicalServicesRepository.create(newService);
    return result;
}

export async function getFilteredData(filter: ServiceFilter) {
    // payload;
    const result = await medicalServicesRepository.getFilteredData(filter);
    return result;
};
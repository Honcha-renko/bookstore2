import express from 'express';
import { Response, Request } from "express";
import { Registration } from '../../shared/models/interfaces/registration';
import * as userService from '../services/order.service';
import { UserFilter } from '../../shared/models/interfaces/filters/user.filter';
import { ObjectId } from 'mongodb';
import * as orderService from '../services/order.service';



export async function getFilteredDataAsync(req: Request, res: Response) {
  //let result = await userService.getFilteredDataAsync(req.body);
  //res.send(result);
}

export async function getById(id: string | ObjectId) {

}

export async function create(req: Request, res: Response) {
  const result = orderService.create(req.body);
  res.send(result);
}

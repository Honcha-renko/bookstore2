import express from "express";
import * as userHandler from '../orders/handlers/order.user.handler';
import * as adminHandler from '../orders/handlers/order.admin.handler';

//import * as doctorHandler from '../orders/handlers/order.admin.handler';//is this neccesary?

export const userRouter = express.Router();
userRouter
    .get(':id')
    .post('', userHandler.create);

export const adminRouter = express.Router();
adminRouter
    .get('/getFiltered', adminHandler.getFilteredDataAsync)
    .get(':id', userHandler.getById)
    .delete(':id', adminHandler.markAsRemoved);

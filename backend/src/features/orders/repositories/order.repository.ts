import { orderModel } from '../../shared/models/orders';
import mongoose from '../../../dataAccess/connection';
import Order from '../../shared/models/interfaces/order';

export function getFilteredData() {
    let orders = orderModel.find().select('_id').populate('Order.item.serviseId')//???,
    return orders;
}

export async function create(order: Order & mongoose.Document) {
    const result = await order.save();
    if (result.errors) {
        return false;
    }
    return true;
}
export enum ServiceSortColumn {
    name = 1,
    serviceType = 2,
    price = 3,
}
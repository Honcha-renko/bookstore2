export enum Currency {
    USD = 0,
    EUR = 1,
    JPY = 2,
    GBP = 3,
};
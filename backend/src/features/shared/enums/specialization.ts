export enum Specialization {
    surgery = 1,
    dermatology = 2,
    anesthesiology = 3,
    Neurology = 4,
}
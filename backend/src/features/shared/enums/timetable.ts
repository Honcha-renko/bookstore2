export enum Time {
    none = 0,
    '9-10' = 1,
    '10-11' = 2,
    '11-12' = 3,
}
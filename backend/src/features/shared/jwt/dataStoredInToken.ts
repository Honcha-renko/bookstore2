import { ObjectId } from "mongodb";
import { Roles } from "../enums/roles";

export interface DataStoredInToken {
    id: ObjectId |string;
    role: Roles;
    email: string;
  }
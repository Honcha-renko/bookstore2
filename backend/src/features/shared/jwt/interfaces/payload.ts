import { Roles } from "../../enums/roles";

export interface Payload {
    id: string,
    role: Roles,
    type: string,
    tokenId: string,
} 
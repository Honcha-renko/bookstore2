import mongoose from '../../../../dataAccess/connection';

const TokenSchema = new mongoose.Schema({
    tokenId:String,
    userId:String,
});

export default mongoose.model('token',TokenSchema);
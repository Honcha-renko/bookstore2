import mongoose from '../../../dataAccess/connection';
import { Currency } from '../enums/currency';
import Order from './interfaces/order';
let Schema = mongoose.Schema;

const orderSchema = new Schema({
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    paymentId: { type: mongoose.Schema.Types.ObjectId, ref: 'Payment', required: true },//todo Ineed It?
    status: { type: Boolean, required: true },
    items: [{
        serviceId: { type: mongoose.Schema.Types.ObjectId, ref: 'Service', required: true },
        doctorId: { type: mongoose.Schema.Types.ObjectId, ref: 'Doctor', required: true },
        timetableId: { type: mongoose.Schema.Types.ObjectId, ref: 'Timetable', required: true },

        count: { type: Number, required: true },
        currency: { type: Currency, required: true },
        addInfo: { type: String, required: false },
        amount: { type: Number, required: true },
    }]
}, { collection: 'orders' });

export const orderModel = mongoose.model<Order & mongoose.Document>('Order', orderSchema);
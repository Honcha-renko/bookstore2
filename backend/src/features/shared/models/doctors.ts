import mongoose from '../../../dataAccess/connection';
import Doctor from './interfaces/doctor';
let Schema = mongoose.Schema;

let doctorSchema = new Schema({//doctor as different schema?
    name: String,
    
}, { collection: 'doctors' });

let doctorModel = mongoose.model<Doctor & mongoose.Document>('Doctor', doctorSchema);
export default doctorModel;
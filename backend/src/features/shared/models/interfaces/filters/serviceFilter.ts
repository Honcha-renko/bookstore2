import { ServiceSortColumn } from '../../../enums/ServiceSortColumn';
import { SortType } from "../../../enums/sortType";
import { ServiceType } from '../../../enums/serviceType';

export interface ServiceFilter {
    pageNumber: number,
    pageSize: number,
    searchText: string,
    sortType: SortType,
    serviceSortColumn: ServiceSortColumn,
    serviceType: Array<ServiceType>,
}
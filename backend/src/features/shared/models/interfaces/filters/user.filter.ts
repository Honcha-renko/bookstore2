import { SortType } from "../../../enums/sortType";
import { UserSortColumn } from "../../../enums/UserSortColumn";

export interface UserFilter {
    pageNumber: number,
    pageSize: number,
    searchText: string,
    sortType: SortType,
    userSortColumn: UserSortColumn,
}
import { ServiceType } from "../../enums/serviceType";
import mongoose from "../../../../dataAccess/connection";
import { Currency } from "../../enums/currency";

export interface MedicalServices {
    _id: string | mongoose.Schema.Types.ObjectId,
    name: string,
    type: ServiceType
    description: string,
    doctor_Ids: mongoose.Schema.Types.ObjectId[],
    price: number,
    currency: Currency,

    addInfo: string,
    coverImage: string,
    removedAt: boolean,
}
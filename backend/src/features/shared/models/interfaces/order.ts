import { ObjectId } from "mongodb";
import { Currency } from "../../enums/currency";

export default interface Order {

    userId: string | ObjectId,
   // paymentId: string | ObjectId,
    status: boolean,
    items: {
        serviceId: string | ObjectId,
        doctorId: string | ObjectId,
        timetableId:string | ObjectId,
        
        count: number,//1 sor service obviosly
        currency: Currency,
        addInfo: string,
        amount: number,
    },
}
export interface ResponseModel{
    data: Array<any>,
    totalCount: number,
}
import * as timetable from '../../enums/timetable';

export interface Timetable {
    date: string,
    time: timetable.Time,
    taken: boolean,
}
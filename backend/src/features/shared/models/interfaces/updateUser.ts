import { ObjectId } from "mongodb";

export interface UpdateUser {
    _id: ObjectId | string,
    email: string,
    avatar: string,
    firstName: string,
    lastName: string,
    adress: string,
    changePasswordBox: boolean,
    password: string,
}
import { ObjectId } from "mongodb";
import { Roles } from "../../enums/roles";

export interface User {
    _id: ObjectId|string,
    
    email: string,
    avatar: string,
    firstName: string,
    lastName: string,
    role: Roles,
    passwordHash: string,
}
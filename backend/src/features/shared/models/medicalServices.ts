import mongoose from '../../../dataAccess/connection';
import { ServiceType } from '../enums/serviceType';
import { Currency } from '../enums/currency'
import { MedicalServices } from './interfaces/medicalServices';
let Schema = mongoose.Schema;

let ServicesSchema = new Schema({
  name: { type: String, required: true, unique: true },
  type: { type: ServiceType, required: true },
  description: { type: String, required: false, unique: false },
  doctor_Ids: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }],
  price: { type: Number, required: true },
  currency: { type: Currency, required: true },

  addInfo: { type: String, required: false },
  coverImage: { type: String, required: false },
  removedAt: { type: Boolean, required: true },

}, { collection: 'services' });

export default mongoose.model<MedicalServices & mongoose.Document>('MedicalServices', ServicesSchema);
import mongoose from '../../../dataAccess/connection';
let Schema = mongoose.Schema;
import { Registration } from './interfaces/registration';

const registrationSchema = new Schema({
    email: { type: String, required: true, unique: true },
    firstName: { type: String, required: true , unique: false},
    lastName: { type: String, required: true, unique: false },
    password: { type: String, required: true, unique: true },
});

let registrationModel = mongoose.model<Registration & mongoose.Document>('Registration', registrationSchema);
export default registrationModel;
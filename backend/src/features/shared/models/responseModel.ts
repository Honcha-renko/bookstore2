import mongoose from '../../../dataAccess/connection';
let Schema = mongoose.Schema;
import { ResponseModel } from './interfaces/responseModel';

const ResponseModelSchema = new Schema({
    data: { type: Array, required: true },
    totalCount: { type: Number, required: false },
});

let responseModel = mongoose.model<ResponseModel & mongoose.Document>('responseModel', ResponseModelSchema);
export default responseModel;
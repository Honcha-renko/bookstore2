import mongoose from '../../../dataAccess/connection';
const Schema = mongoose.Schema;
import { Timetable } from '../models/interfaces/timetable';
import { Time } from '../enums/timetable';

const timetableSchema = new Schema({
    date: { type: String, required: true },//var now = new Date(); var formated_date = now.getDate();
    time: { type: Time, required: true },
    taken: { type: Boolean, required: true },
}, { collection: 'timetable' });

const timetableModel = mongoose.model<Timetable & mongoose.Document>('Timetable', timetableSchema);
export = timetableModel;
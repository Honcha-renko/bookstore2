import mongoose from '../../../dataAccess/connection';
const Schema = mongoose.Schema;
import { User } from './interfaces/user';
import { Roles } from '../enums/roles';
import { Specialization } from '../enums/specialization';

const userSchema = new Schema({
    email: { type: String, required: true, unique: true },
    passwordHash: { type: String, required: true, unique: true },

    avatar: { type: String, required: false, unique: false },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    role: { type: Roles, required: true },
    adress: { type: String, required: true, unique: false },

    specialization: { type: Specialization, unique: false },
    timetableId: { type: mongoose.Schema.Types.ObjectId, ref: 'Timetable' },
}, { collection: 'users' });

const userModel = mongoose.model<User & mongoose.Document>('User', userSchema);
export = userModel;

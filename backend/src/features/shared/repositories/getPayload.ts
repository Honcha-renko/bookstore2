import { Request } from "express";
import { Payload } from "../jwt/interfaces/payload";
import { config } from "../../../config";
import jwt from 'jsonwebtoken';

export function getPayload(req: Request) {
    const token = req.get('Authorization').replace('Bearer', '').trimLeft();
    const payload = <Payload>jwt.verify(token, config.jwtSecret);
    return payload;
}
import mongoose from "src/dataAccess/connection";
import { User } from "../models/interfaces/user";
import { UserFilter } from "../models/interfaces/filters/user.filter";

export async function paginateData(users: (Object & mongoose.Document)[], filter: UserFilter): Promise<Object> {
  const offset = (filter.pageNumber- 1) * filter.pageSize;
  const paginatedData = users.slice(offset, offset + filter.pageSize);

    return {data:paginatedData,totalCount:users.length, totalPages: Math.ceil(users.length / filter.pageSize)};
};

    
    // if (filter.sortType != 0) { //it is possible to make a chose if sorting?
    //     users.sort(function (a, b) {
    //         if (a.get(`${property}`) < b.get(`${property}`)) { return -1; }
    //         if (a.get(`${property}`) > b.get(`${property}`)) { return 1; }
    //         return 0;
    //     });
    // }

    //  await users.sort(u=>u.get(`${property}`)).slice((filter.pageNumber-1)*filter.pageSize).limit(10);
    //mongoose-paginate
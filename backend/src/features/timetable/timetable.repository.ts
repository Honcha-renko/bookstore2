import { Timetable } from "../shared/models/interfaces/timetable";
import mongoose from "../../dataAccess/connection";
import timetableModel from '../shared/models/timetable';

export async function createTimetable(timetable: Timetable & mongoose.Document) {
    const userCheck = await timetableModel.findOne({ date: timetable.date } && { time: timetable.time });
    if (userCheck !== null) {
        return false;
    }
    const result = await timetable.save();
    if (result.errors) {
        return false;
    }
    return true;
};
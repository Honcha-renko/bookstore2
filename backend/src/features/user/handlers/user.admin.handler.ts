import { Request, Response } from "express";
import * as userService from '../services/user.service';
import { getPayload } from '../../shared/repositories/getPayload';


export async function getFilteredDataAsync(req: Request, res: Response) {
    const payload = getPayload(req);
    
    let result = await userService.getFilteredDataAsync(payload, req.body);
    res.send(result);
}
import { Request, Response } from "express";
import * as Token from "../../auth/jwtHelper/jwtHelper";
import * as userService from '../services/user.service';
import { Payload } from "../../shared/jwt/interfaces/payload";
import { config } from "../../../config";
import jwt from 'jsonwebtoken';

export async function signUpAsync(req: Request, res: Response) {
    const resultModel = await userService.signUpAsync(req.body);
    if (resultModel === true) {
        const user = await userService.findByEmail(req.body.email);
        const tokenData = Token.createTokens(user);

        res.send(tokenData);
    }
    res.send(resultModel);
};

export async function signIn(req: Request, res: Response) {
    const result = await userService.signInAsync(req.body);
    if (result) {
        const user = await userService.findByEmail(req.body.email);
        const tokenData = Token.createTokens(user);
        const replaceToken = await Token.replaceToken(tokenData.tokenId, user.id);
        res.setHeader('accessToken', tokenData.accessToken);
        res.setHeader('refreshToken', tokenData.refreshToken);
        return res.send(tokenData);
    }
    return res.send(result);
}

export async function update(req: Request, res: Response) {
    const header = req.get('Authorization');
    const token = header.replace('Bearer', '').trimLeft();
    const payload = <Payload>jwt.verify(token, config.jwtSecret);
    const result = await userService.update(payload, req.body);
    return res.send(result);
}

export async function signOut(req: Request, res: Response) {
    const result = await userService.signOut(req.body._id);
    return res.send(result);
}
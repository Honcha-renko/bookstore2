import express from 'express';
import * as userHandler from './handlers/user.user.handler';
import * as adminHandler from './handlers/user.admin.handler';
import * as authMiddleware from '../auth/jwtHelper/jwtHelper';

export const userRouter = express.Router();

userRouter
    .post('/signUp', userHandler.signUpAsync)
    .post('/me', userHandler.signIn)
    .post('/update', authMiddleware.authMiddleware, userHandler.update)
    .post('/logOut', authMiddleware.authMiddleware, userHandler.signOut)
    .post('/refreshTokens', authMiddleware.refreshToken);

export const adminRouter = express.Router();
adminRouter
    .get('/getFilteredData', authMiddleware.authMiddleware, adminHandler.getFilteredDataAsync)
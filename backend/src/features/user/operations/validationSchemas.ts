import registrationSchema from './registrationRequest.schema.json';
import updateSchema from './updateValidation.schema.json';
import getFilteredSchema from './getFilteredUsersValidation.Schema.json';
import  signInSchema from './signInValidationSchema.json';

export = {
    registrationSchema,
    updateSchema,
    getFilteredSchema,
    signInSchema,
}
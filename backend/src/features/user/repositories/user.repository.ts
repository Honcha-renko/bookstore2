import bcrypt from 'bcrypt';
import mongoose from '../../../dataAccess/connection';
import { Roles } from '../../shared/enums/roles';
import { UserSortColumn } from '../../shared/enums/UserSortColumn';
import { UpdateUser } from '../../shared/models/interfaces/updateUser';
import { User } from '../../shared/models/interfaces/user';
import { UserFilter } from '../../shared/models/interfaces/filters/user.filter';
import * as user from '../../shared/models/user.schema';
let User = user.default;


export async function getFilteredDataAsync(filter: UserFilter): Promise<Object> {
    const property = UserSortColumn[filter.userSortColumn];
    let sort = { [property]: filter.sortType };

    let users = User.find({ role: Roles.user });

    if (!filter.searchText.startsWith(' ') && filter.searchText.length != 0) {
        users = users.find({ firstName: filter.searchText } || { lastName: filter.searchText } || { email: filter.searchText });
    }

    users.sort(sort);
    const totalCount = (await users).length
    users.skip((filter.pageNumber - 1) * filter.pageSize);
    const data = await users.limit(filter.pageSize);

    return { data, totalCount };
};

export async function findByEmail(email: string): Promise<User & mongoose.Document> {
    const user = await User.findOne({ email: email });
    return user;
}

export async function findById(id: any): Promise<User & mongoose.Document> {
    const user = await User.findById(id);
    return user;
}

export async function update(user: UpdateUser): Promise<User & mongoose.Document> {
    let result = await User.updateOne({ _id: user._id }, { '$set': user });
    if (user.changePasswordBox) {
        result = await User.updateOne({ _id: user._id }, { passwordHash: await bcrypt.hash(user.password, 10) });
    }
    return result;
}

import bcrypt from 'bcrypt';
import mongoose from '../../../dataAccess/connection';
import { checkRole } from "../../auth/checkRoleHelper/checkRoleHelper";
import * as  authMiddleware from '../../auth/jwtHelper/jwtHelper';
import signUp from '../../auth/registration/registration';
import { signIn } from '../../auth/signIn/signIn';
import { Roles } from '../../shared/enums/roles';
import { Payload } from '../../shared/jwt/interfaces/payload';
import { UserFilter } from '../../shared/models/interfaces/filters/user.filter';
import { Registration } from '../../shared/models/interfaces/registration';
import { SignIn } from '../../shared/models/interfaces/signIn';
import { UpdateUser } from '../../shared/models/interfaces/updateUser';
import logger from "../../utils/logger";
import { validateWithSchema } from '../../utils/validateWithSchema';
import schemas from '../operations/validationSchemas';
import * as userRepository from '../repositories/user.repository';
import { User } from '../../shared/models/interfaces/user';
import * as userModel from '../../shared/models/user.schema';
let User = userModel.default;


export async function signUpAsync(registrationData: Registration): Promise<Object> {

    logger.log('info', `>>>>userService.signUp(), with: user = ${JSON.stringify(registrationData)}`);
    const validationResult = validateWithSchema(registrationData, schemas.registrationSchema);

    if (!validationResult.valid) {
        logger.log('error', `>>>>userService.signUp(), invalid data: ${validationResult.errors}`);
        return { message: "Invalid request", errors: validationResult.errors };
    }

    const user = new User({
        email: registrationData.email,
        firstName: registrationData.firstName,
        lastName: registrationData.lastName,
        passwordHash: await bcrypt.hash(registrationData.password, 10),
        adress: registrationData.adress,
        role: Roles.user,
    });
    const result = await signUp(user);
    if (!result) {
        logger.log('error', `<<<< userService.signUp(), result =  ${result}`);
        return "User with this email is already exist";
    };
    return result;
};

export async function getFilteredDataAsync(payload: Payload, filter: UserFilter): Promise<Object> {

    logger.log('info', `>>>>userService.getFilteredDataAsync(), with: filter = ${JSON.stringify(filter)}`);
    const validationResult = validateWithSchema(filter, schemas.getFilteredSchema);

    if (!validationResult.valid) {
        logger.log('error', `>>>>userService.getFilteredDataAsync(), invalid data: ${validationResult.errors}`);
        return { message: "Invalid request", errors: validationResult.errors };
    }

    const result = checkRole(payload.role, [1]);
    if (!result) {
        logger.log('error', `<<<< userService.getFilteredDataAsync(), result =  ${result}`);
        return "Access denied.";
    }
    const data = await userRepository.getFilteredDataAsync(filter);
    return data;
};

export async function signInAsync(signInModel: SignIn): Promise<Object> {
    logger.log('info', `>>>>userService.signInAsync(), with: signInModel = ${JSON.stringify(signInModel)}`);
    const validationResult = validateWithSchema(signInModel, schemas.signInSchema);

    if (!validationResult.valid) {
        logger.log('error', `>>>>userService.signInAsync(), invalid data: ${validationResult.errors}`);
        return { message: "Invalid request", errors: validationResult.errors };
    }

    const result = await signIn(signInModel);

    if (!result) {
        logger.log('error', `<<<< signIn(), result =  ${result}`);
    }
    return result;
};

export async function findByEmail(email: string): Promise<User & mongoose.Document> {
    const user = await userRepository.findByEmail(email);
    return user;
};

export async function update(payload: Payload, user: UpdateUser): Promise<Object> {
    logger.log('info', `>>>>userService.update(), with: user = ${JSON.stringify(user)}`);
    const validationResult = validateWithSchema(user, schemas.updateSchema);

    if (!validationResult.valid) {
        logger.log('error', `>>>>userService.update(), invalid data: ${validationResult.errors}`);
        return { message: "Invalid request", errors: validationResult.errors };
    }
    const checkResult = checkRole(payload.role, [1, 2, 3]);
    if (checkResult) {
        logger.log('error', `<<<< userService.update(), result =  ${checkResult}`)
        return "Access denied.";
    }
    const result = await userRepository.update(user);
    if (result.errors) {
        logger.log('error', `update(), result = ${JSON.stringify(result)}`);
    }
    return result;
}

export async function signOut(userId: string) {
    const result = await authMiddleware.signOut(userId);

    if (result.deletedCount == 0) {
        return false;
    }
    return true;
}
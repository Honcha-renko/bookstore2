import winston from "winston";
const format = winston.format;

const logger = winston.createLogger({
    format: winston.format.combine(
        format.timestamp(),
        format.errors({ stack: true }),
        format.json(),
        format.prettyPrint(),
    ),
    defaultMeta: { service: 'user-service' },
    transports: [
        new winston.transports.File({ level: 'info',  filename: 'info.log', }),
        new winston.transports.File({ level: 'error', filename: 'error.log', }),
    ],
    exitOnError: false,
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        handleExceptions: true,
        format: format.combine(
            format.colorize(),
            format.simple()
        )
    }));
}
export default logger;
import { validate } from "jsonschema";

export function validateWithSchema(value: any, schema: any){
    return validate (value, schema);
}